# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number_to_guess = rand(1..100)
  attempted_guesses = 1
  puts "guess a number"
  guess = gets.chomp.to_i

  until guess == number_to_guess do
    if guess > number_to_guess
      puts "#{guess} is too high"
    else
      puts "#{guess} is too low"
    end

    puts "guess a number"
    guess = gets.chomp.to_i
    attempted_guesses += 1
  end

  print "It took you #{attempted_guesses} to guess #{number_to_guess}"
end
